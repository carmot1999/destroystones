﻿using UnityEngine;

public class Stone : MonoBehaviour
{
    private const float yDie = -30.0f;

    public GameObject explosion;

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y < yDie)
        {
            Destroy(gameObject);
        }
    }

    private void OnMouseDown()
    {
        Instantiate(explosion, transform.position, Quaternion.identity);

        if (gameObject.name.Contains("4"))
        {
            GameManager.currentNumberDestroyedStones--;
        }

        else
        {
            GameManager.currentNumberDestroyedStones++;
        }

        Destroy(gameObject);
    }
}
